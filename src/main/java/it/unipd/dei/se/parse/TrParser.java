/*
 *  Copyright 2017-2021 University of Padua, Italy
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package it.unipd.dei.se.parse;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Provides a very basic parser for the TIPSTER corpus (FBIS, FR, FT, LATIMES), i.e. TREC Disks 4 and 5 minus
 * Congressional Record.
 * <p>
 * It is based on the parser <a href="https://github.com/isoboroff/trec-demo/blob/master/src/TrecDocIterator.java"
 * target="_blank">TrecDocIterator.java</a> by Ian Soboroff.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public class TrParser extends it.unipd.dei.se.parse.DocumentParser {

    /**
     * The size of the buffer for the body element.
     */
    private static final int BODY_SIZE = 1024 * 8;

    /**
     * The currently parsed document
     */
    private it.unipd.dei.se.parse.ParsedDocument document = null;
    private ObjectMapper objectMapper = null;
    private JsonFactory factory = null;
    private JsonParser parser = null;


    /**
     * Creates a new TIPSTER Corpus document parser.
     *
     * @param in the reader to the document(s) to be parsed.
     * @throws NullPointerException     if {@code in} is {@code null}.
     * @throws IllegalArgumentException if any error occurs while creating the parser.
     */
    public TrParser(final Reader in) throws IOException {
        super(new BufferedReader(in));

        this.objectMapper = new ObjectMapper();
        this.factory = objectMapper.getFactory();

        try {
            parser = factory.createParser(in);
        } catch (IOException e) {
            e.printStackTrace();
        }

        parser.nextToken(); // {
        parser.nextToken(); //   arguments
        parser.nextToken(); //           [
        parser.nextToken(); //               {
        parser.nextToken(); //                   id
    }

    @Override
    public boolean hasNext() {
       try {

           document = new ParsedDocument();

           while (!parser.isClosed()) {
               String tmpValue = parser.getCurrentName();

               if ("id".equals(tmpValue)) {
                   parser.nextToken();
                   String id = parser.getText();
                   document.setId(id);
               }

               else if ("context".equals(tmpValue)) {
                   parser.nextToken(); // {
                   parser.nextToken(); // value, as instance acquisitionTime
                   String contextKey = parser.getCurrentName();


                   while (!"sourceText".equals(contextKey)) {
                       contextKey = parser.getCurrentName();
                       parser.nextToken();
                   }

                   String sourceText = parser.getText();
                   document.setDocSourceText(sourceText);

                   // skipping other child elements of context...
                   while (!parser.nextToken().equals(JsonToken.END_OBJECT)) {
                       ;
                   }

                   if (parser.nextToken().equals(JsonToken.END_OBJECT)) {
                       return true;
                   }
               }

               parser.nextToken();
           }

           return false;

       } catch (IOException e) {
            throw new IllegalStateException("Unable to parse the document.", e);
       }
    }

    @Override
    protected final it.unipd.dei.se.parse.ParsedDocument parse() {
        return document;
    }


    /**
     * Main method of the class. Just for testing purposes.
     *
     * @param args command line arguments.
     * @throws Exception if something goes wrong while indexing.
     */
    public static void main(String[] args) throws Exception {

        Reader reader = new FileReader(
               // "/Users/Nicola Maino/Documents/UNIPD/Search Engines/seupd2021-tr/DOC/parliamentary.json");
                //"D:/Users/Gabriela/Documents/Gabriella/UNIPD/Magistrale/Search Engines/Touche 2020/debatepedia.json");
                "/home/daniel/Scrivania/project/seupd2021-tr/DOC/parliamentary.json");
        TrParser p = new TrParser(reader);

        for (it.unipd.dei.se.parse.ParsedDocument d : p) {
            System.out.printf("%n%n------------------------------------%n%s%n%n%n", d.toString());
        }

    }

}
