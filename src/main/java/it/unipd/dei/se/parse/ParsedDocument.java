/*
 *  Copyright 2021 University of Padua, Italy
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package it.unipd.dei.se.parse;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.lucene.document.Field;

/**
 * Represents a parsed document to be indexed.
 *
 * @author Nicola Ferro (ferro@dei.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public class ParsedDocument {

    /**
     * The names of the {@link Field}s within the index.
     *
     * @author Nicola Ferro
     * @version 1.00
     * @since 1.00
     */
    public final static class FIELDS {

        // arguments
        public static final String ID = "id";
        public static final String CONCLUSION = "conclusion";

        // premises
        public static final String TEXT = "text";
        public static final String STANCE = "stance";

        // context
        public static final String ACQUISITION_TIME = "acquisitionTime";
        public static final String DISCUSSION_TITLE = "discussionTitle";
        public static final String MODE = "mode";
        public static final String SOURCE_DOMAIN = "sourceDomain";
        public static final String SOURCE_ID = "sourceId";
        public static final String SOURCE_TEXT = "sourceText";
        public static final String SOURCE_TEXT_CONCLUSION_START = "sourceTextConclusionStart";
        public static final String SOURCE_TEXT_CONCLUSION_END = "sourceTextConlusionEnd";
        public static final String SOURCE_TEXT_PREMISE_START = "sourceTextPremiseStart";
        public static final String SOURCE_TEXT_PREMISE_END = "sourceTextPremiseEnd";
        public static final String SOURCE_TITLE = "sourceTitle";
        public static final String SOURCE_URL = "sourceURL";
        public static final String AUTHOR = "author";
        public static final String AUTHOR_IMAGE = "authorImage";
        public static final String AUTHOR_ORGANIZATION = "authorOrganization";
        public static final String AUTOR_ROLE = "authorRole";
        public static final String DATE = "date";
        //public static final String MODE = "mode";
        public static final String TOPIC = "topic";
        public static final String NAME = "name";
        public static final String WEIGHT = "weight";
        public static final String NORMALIZED_WEIGHT = "normalizedWeight";
        public static final String RANK = "rank";
    }

    private  String id;
    private  String conclusion;
    private  String text;
    private  String stance;
    private  String acquisitionTime;
    private  String discussionTitle;
    private  String mode;
    private  String sourceDomain;
    private  String sourceId;
    private  String sourceText;
    private  String sourceTextConclusionStart;
    private  String sourceTextConlusionEnd;
    private  String sourceTextPremiseStart;
    private  String sourceTextPremiseEnd;
    private  String sourceTitle;
    private  String sourceURL;
    private  String author;
    private  String authorImage;
    private  String authorOrganization;
    private  String authorRole;
    private  String date;
    private  String topic;
    private  String name;
    private  String weight;
    private  String normalizedWeight;
    private  String rank;

    public ParsedDocument() {
        this.id = "UNASSIGNED_ID";
        this.sourceText = "UNASSIGNED_TEXT";
    }

    public ParsedDocument(final String id) {
        if (id == null) {
            throw new NullPointerException("Document identifier cannot be null.");
        }

        this.id = id;
    }

    /**
     * Creates a new parsed document
     *
     * @param id   the unique document identifier.
     * @param body the body of the document.
     * @throws NullPointerException  if {@code id} and/or {@code body} are {@code null}.
     * @throws IllegalStateException if {@code id} and/or {@code body} are empty.
     */

    // TODO: conclusion might not be useful -> remove check if
    // TODO: stance might not be useful -> remove check if
    // same for discussiontitle acquisitiontime mode sourcedomain
    public ParsedDocument(final String id, final String conclusion, final String text, final String stance,
                          final String acquisitionTime, final String discussionTitle, final String mode,
                          final String sourceDomain, final String sourceId, final String sourceText,
                          final String sourceTextConclusionStart, final String sourceTextConlusionEnd,
                          final String sourceTextPremiseStart, final String sourceTextPremiseEnd,
                          final String sourceTitle, final String sourceURL, final String author,
                          final String authorImage, final String authorOrganization, final String authorRole,
                          final String date,  final String topic, final String name,
                          final String weight, final String normalizedWeight, final String rank) {

        if (id == null) {
            throw new NullPointerException("Document identifier cannot be null.");
        }

        if (id.isEmpty()) {
            throw new IllegalStateException("Document identifier cannot be empty.");
        }

        this.id = id;

        if (conclusion == null) {
            throw new NullPointerException("Document conclusion cannot be null.");
        }

        if (conclusion.isEmpty()) {
            throw new IllegalStateException("Document conclusion cannot be empty.");
        }

        this.conclusion = conclusion;

        if (text == null) {
            throw new NullPointerException("Document text cannot be null.");
        }

        if (text.isEmpty()) {
            throw new IllegalStateException("Document text cannot be empty.");
        }

        this.text = text;

        if (stance == null) {
            throw new NullPointerException("Document stance cannot be null.");
        }

        if (stance.isEmpty()) {
            throw new IllegalStateException("Document stance cannot be empty.");
        }

        this.stance = stance;

        if (acquisitionTime == null) {
            throw new NullPointerException("Document acquisitionTime cannot be null.");
        }

        if (acquisitionTime.isEmpty()) {
            throw new IllegalStateException("Document acquisitionTime cannot be empty.");
        }

        this.acquisitionTime = acquisitionTime;

        if (discussionTitle == null) {
            throw new NullPointerException("Document discussionTitle cannot be null.");
        }

        if (discussionTitle.isEmpty()) {
            throw new IllegalStateException("Document discussionTitle cannot be empty.");
        }

        this.discussionTitle = discussionTitle;

        if (mode == null) {
            throw new NullPointerException("Document mode cannot be null.");
        }

        if (mode.isEmpty()) {
            throw new IllegalStateException("Document mode cannot be empty.");
        }

        this.mode = mode;

        if (sourceDomain == null) {
            throw new NullPointerException("Document sourceDomain cannot be null.");
        }

        if (sourceDomain.isEmpty()) {
            throw new IllegalStateException("Document sourceDomain cannot be empty.");
        }

        this.sourceDomain = sourceDomain;

        if (sourceId == null) {
            throw new NullPointerException("Document sourceId cannot be null.");
        }

        if (sourceId.isEmpty()) {
            throw new IllegalStateException("Document sourceId cannot be empty.");
        }

        this.sourceId = sourceId;

        if (sourceText == null) {
            throw new NullPointerException("Document sourceText cannot be null.");
        }

        if (sourceText.isEmpty()) {
            throw new IllegalStateException("Document sourceText cannot be empty.");
        }

        this.sourceText = sourceText;

        if (sourceTextConclusionStart == null) {
            throw new NullPointerException("Document sourceTextConclusionStart cannot be null.");
        }

        if (sourceTextConclusionStart.isEmpty()) {
            throw new IllegalStateException("Document sourceTextConclusionStart cannot be empty.");
        }

        this.sourceTextConclusionStart = sourceTextConclusionStart;

        if (sourceTextConlusionEnd == null) {
            throw new NullPointerException("Document sourceTextConclusionEnd cannot be null.");
        }

        if (sourceTextConlusionEnd.isEmpty()) {
            throw new IllegalStateException("Document sourceTextConclusionEnd cannot be empty.");
        }

        this.sourceTextConlusionEnd = sourceTextConlusionEnd;

        if (sourceTextPremiseStart == null) {
            throw new NullPointerException("Document sourceTextPremiseStart cannot be null.");
        }

        if (sourceTextPremiseStart.isEmpty()) {
            throw new IllegalStateException("Document sourceTextPremiseStart cannot be empty.");
        }

        this.sourceTextPremiseStart = sourceTextPremiseStart;

        if (sourceTextPremiseEnd == null) {
            throw new NullPointerException("Document sourceTextPremiseEnd cannot be null.");
        }

        if (sourceTextPremiseEnd.isEmpty()) {
            throw new IllegalStateException("Document sourceTextPremiseEnd cannot be empty.");
        }

        this.sourceTextPremiseEnd = sourceTextPremiseEnd;

        if (sourceTitle == null) {
            throw new NullPointerException("Document sourceTitle cannot be null.");
        }

        if (sourceTitle.isEmpty()) {
            throw new IllegalStateException("Document sourceTitle cannot be empty.");
        }

        this.sourceTitle = sourceTitle;

        if (sourceURL == null) {
            throw new NullPointerException("Document sourceUrl cannot be null.");
        }

        if (sourceURL.isEmpty()) {
            throw new IllegalStateException("Document sourceUrl cannot be empty.");
        }

        this.sourceURL = sourceURL;

        if (author == null) {
            throw new NullPointerException("Document author cannot be null.");
        }

        if (author.isEmpty()) {
            throw new IllegalStateException("Document author cannot be empty.");
        }

        this.author = author;

        if (authorImage == null) {
            throw new NullPointerException("Document authorImage cannot be null.");
        }

        if (authorImage.isEmpty()) {
            throw new IllegalStateException("Document authorImage cannot be empty.");
        }

        this.authorImage = authorImage;

        if (authorOrganization == null) {
            throw new NullPointerException("Document authorOrganization cannot be null.");
        }

        if (authorOrganization.isEmpty()) {
            throw new IllegalStateException("Document authorOrganization cannot be empty.");
        }

        this.authorOrganization = authorOrganization;

        if (authorRole == null) {
            throw new NullPointerException("Document authorRole cannot be null.");
        }

        if (authorRole.isEmpty()) {
            throw new IllegalStateException("Document authorRole cannot be empty.");
        }

        this.authorRole = authorRole;
        this.date = date;

        this.topic = topic;
        this.name = name;
        this.weight = weight;
        this.normalizedWeight = normalizedWeight;
        this.rank = rank;
    }

    /**
     * Returns the unique document identifier.
     *
     * @return the unique document identifier.
     */
    public String getIdentifier() {
        return id;
    }



    /**
     * Returns the body of the document.
     *
     * @return the body of the document.
     */
    public String getBody() {
        return sourceText;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDocSourceText(String text) {
        this.sourceText = text;
    }


    @Override
    public final String toString() {
        ToStringBuilder tsb = new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE).append("identifier", id).append("text", sourceText);

        return tsb.toString();
    }

    @Override
    public final boolean equals(Object o) {
        return (this == o) || ((o instanceof ParsedDocument) && id.equals(((ParsedDocument) o).id));
    }

    @Override
    public final int hashCode() {
        return 37 * id.hashCode();
    }

}
