package it.unipd.dei.se.search;

public class TopicElements {

    private int number;
    private String title;
    private String description;
    private String narrative;


    public TopicElements(int number, String title, String description, String narrative) {

        this.number = number;
        this.title = title;
        this.description = description;
        this.narrative = narrative;

    }

    public int getNumber(){
        return number;
    }

    public String getTitle(){
        return title;
    }

    public String getDescription(){
        return description;
    }

    public String getNarrative(){
        return narrative;
    }

    public void setNumber(int number){
        this.number = number;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setNarrative(String narrative) {
        this.narrative = narrative;
    }
}
