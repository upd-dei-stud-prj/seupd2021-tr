package it.unipd.dei.se.search;


import org.apache.lucene.benchmark.quality.QualityQuery;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

// TODO: Cleaning of the fields narrative ad description which might have newlines
public class TrTopicsLoader {

    public static QualityQuery[] readTopic(BufferedReader reader) throws XMLStreamException, FileNotFoundException {

        ArrayList<QualityQuery> res = new ArrayList<>();
        XMLInputFactory xmlif = XMLInputFactory.newFactory();

        XMLStreamReader xml = null;

        try {
            xml = xmlif.createXMLStreamReader(reader);
        }
        catch (XMLStreamException e) {
            e.printStackTrace();
        }

        int eventType;
        HashMap<String, String> element = null;
        TopicElements topicElements = new TopicElements(0,"","","");

        while (xml.getEventType() != XMLStreamConstants.END_DOCUMENT) {

            eventType = xml.next();

            if (eventType == XMLStreamConstants.START_ELEMENT) {

                String tmp = String.valueOf(xml.getName());

                switch (tmp) {
                    case "number" -> {
                        xml.next();
                        topicElements.setNumber(Integer.parseInt(xml.getText()));
                        continue;
                    }
                    case "title" -> {
                        xml.next();
                        topicElements.setTitle(xml.getText());
                        continue;
                    }
                    case "description" -> {
                        xml.next();
                        topicElements.setDescription(xml.getText());
                        continue;
                    }
                    case "narrative" -> {
                        eventType = xml.next();
                        topicElements.setNarrative(xml.getText());
                        continue;
                    }
                }

                // writes
                if(topicElements.getTitle().length() !=0) {
                    element = new HashMap<>();
                    element.put("title", topicElements.getTitle());
                    element.put("description", topicElements.getDescription());
                    element.put("narrative", topicElements.getNarrative());

                    QualityQuery topic = new QualityQuery(topicElements.getNumber() + "", element);
                    res.add(topic);
                }
            }

            // last write
            if (eventType == XMLStreamConstants.END_DOCUMENT) {
                element = new HashMap<>();
                element.put("title", topicElements.getTitle());
                element.put("description", topicElements.getDescription());
                element.put("narrative", topicElements.getNarrative());

                QualityQuery topic = new QualityQuery(topicElements.getNumber() + "", element);
                res.add(topic);
            }
        }

        QualityQuery[] queriesArray= res.toArray(new QualityQuery[res.size()]);

        for(int i=0; i<queriesArray.length;i++) {
            System.out.printf("################ number %s\n", queriesArray[i].getQueryID());
            System.out.printf("################ title %s\n", queriesArray[i].getValue("title"));
            System.out.printf("################ description %s\n\n", queriesArray[i].getValue("description"));
        }

        return queriesArray;
    }

    public static void main(String[] args) throws IOException, XMLStreamException {

        // Reader reader = new FileReader("/home/daniel/Scrivania/topics-task-1.xml");
        Reader reader = new FileReader("/Users/Nicola Maino/Documents/UNIPD/Search Engines/seupd2021-tr/TOPICS/topics-task-1.xml");

        BufferedReader buffer = new BufferedReader(reader);
        readTopic(buffer);

    }

}
