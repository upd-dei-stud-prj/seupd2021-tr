# Touchè Rechercer Homework 1
This project is our personal solution of "Touché Task 1: Argument Retrieval for Controversial Questions" for The CLEF Initiative.

##### Team Memebers:
* Daniel Cavallaro
* Gabriella Buosi
* Nicola Maino


## General info
The project is organaized in several part. In each of them are develop a different task for the Information Retrival (text search version) purpose.
All part have a class with a main function for the debugging purpose.
##### Parse part
It parse all the .json that are files that are located in  /DOC folder. For all document that find in each file it saves their 'id' and 'sourceText' on a ParsedDocument objects.

##### Analyzer part
It implement a customized analyzer that in wich are implement variuos filters for the tokens that are found.
##### Index part
It index all the ParsedDocument objetcs that the parse parse part have create.

##### Search part
It parse the .xml file that are located in the /TOPICS folder. For all query that find it saved their feature (topic, description and narrative).
Finally, it search the relevant documents for every query that find on.
##### TouchèRechercer main part
It implement the main function that had to be runned for the task 1 of the CLEF initiative.

## Tecnologies
* java
* lucene
